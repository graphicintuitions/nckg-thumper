<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require '../vendor/autoload.php';

// set config
$config = array(
    'config' => array(
        'nckg/thumper::asset_path' => '/assets',
        'nckg/thumper::realpath' => realpath(__DIR__) . '/assets',
        'nckg/thumper::quality' => 90,
        'nckg/thumper::fit' => 1,
    )
);

$thumper = new Nckg\Thumper\Thumper($config);
$image = $thumper->process($_SERVER['REQUEST_URI']);
$thumper->serve($image->dirname . '/' . $image->basename);