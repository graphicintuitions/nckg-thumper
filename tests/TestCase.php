<?php

use org\bovigo\vfs\vfsStream;

class TestCase extends PHPUnit_Framework_TestCase {
    /**
     * @var  vfsStreamDirectory
     */
    protected $root;

    /**
     *
     * array
     */
    protected $config;

    /**
     * Set up for each test
     */
    public function setUp()
    {
        // setup
        $this->root = vfsStream::setup('assets');

        // Copy directory structure from file system
        vfsStream::copyFromFileSystem(__DIR__ . '/assets');

        // set config
        $this->config = array(
            'config' => array(
                'nckg/thumper::asset_path' => vfsStream::url('assets'),
                'nckg/thumper::realpath' => vfsStream::url('assets'),
                'nckg/thumper::quality' => 50,
                'nckg/thumper::fit' => 1,
            )
        );
    }
}