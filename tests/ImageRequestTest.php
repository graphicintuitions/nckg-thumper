<?php

use org\bovigo\vfs\vfsStream;
use Nckg\Thumper\ImageRequest;

class ImageRequestTest extends TestCase {
    /**
     *
     * @var Nckg\Thumper\ImageRequest
     */
    protected $object;

    /**
     * Set up for each test
     */
    public function setUp()
    {
        parent::setUp();

        $this->object = new ImageRequest($this->config);
    }

    public function testInitFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->init(vfsStream::url('assets/w50/'));
    }

    public function testNoParameters()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->init(vfsStream::url('assets/happy-kitten.jpg'));
    }

    public function testSetWidthFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setWidth(0);
    }

    public function testSetHeightFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setHeight(0);
    }

    public function testSetQualityFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setQuality(0);
    }

    public function testSetFitFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setFit(-1);
    }

    public function testSetFitFailsSomeMore()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setFit(999);
    }

    public function testSetCropFails()
    {
        $this->setExpectedException('RuntimeException');
        $this->object->setCrop("LOLLERCOPTER");
    }
}