<?php

use org\bovigo\vfs\vfsStream;
use Nckg\Thumper\ImageRequest;
use Nckg\Thumper\Transformer;
use Intervention\Image\Image;

class TransformerTest extends TestCase {
    /**
     *
     * @var ImageRequest
     */
    protected $object;

    /**
     *
     * @var ImageRequest
     */
    protected $request;

    /**
     * Setup for each function
     */
    public function setUp()
    {
        parent::setUp();

        $this->request = new ImageRequest($this->config);
    }

    public function testTransformResizeToFit()
    {
        $url = 'assets/w10-h10-f0/happy-kitten.jpg';
        $this->request->init(vfsStream::url($url));

        $transformer = new Transformer($this->request, new Image(vfsStream::url('assets/happy-kitten.jpg')));
        $image = $transformer->transform();

        $this->assertEquals(10, $image->width);
        $this->assertEquals(10, $image->height);
    }

    public function testTransformCropAndResize()
    {
        $url = 'assets/w20-h10-f1/happy-kitten.jpg';
        $this->request->init(vfsStream::url($url));

        $transformer = new Transformer($this->request, new Image(vfsStream::url('assets/happy-kitten.jpg')));
        $image = $transformer->transform();

        $this->assertEquals(20, $image->width);
        $this->assertEquals(10, $image->height);
    }

    public function testTransformResizeAndFit()
    {
        $url = 'assets/w10-h10-f2/happy-kitten.jpg';
        $this->request->init(vfsStream::url($url));

        $transformer = new Transformer($this->request, new Image(vfsStream::url('assets/happy-kitten.jpg')));
        $image = $transformer->transform();

        $this->assertEquals(10, $image->width);
        $this->assertEquals(10, $image->height);
    }

    public function testTransformResizeAndFitNoBorders()
    {
        $url = 'assets/w10-h10-f3/happy-kitten.jpg';
        $this->request->init(vfsStream::url($url));

        $transformer = new Transformer($this->request, new Image(vfsStream::url('assets/happy-kitten.jpg')));
        $image = $transformer->transform();

        $this->assertEquals(10, $image->width);
        $this->assertEquals(7, $image->height);
    }
}