<?php

use org\bovigo\vfs\vfsStream;
use Nckg\Thumper\Thumper;
use Intervention\Image\Image;

class ThumperTest extends TestCase {
    /**
     *
     * @var Nckg\Thumper\Thumper
     */
    protected $object;

    /**
     * Set up for each test
     */
    public function setUp()
    {
        parent::setUp();

        $this->object = new Thumper($this->config);
    }

    /**
     * Imengine can be instantiated
     */
    public function testInstantiate()
    {
        $this->assertInstanceOf('Nckg\Thumper\Thumper', $this->object);
    }

    /**
     * Test if an image can be processed
     *
     * @return void
     */
    public function testProcess()
    {
        $this->object->process(vfsStream::url('assets/w50/happy-kitten.jpg'));
        $this->assertTrue($this->root->hasChild('w50'));
        $this->assertTrue($this->root->getChild('w50')->hasChild('happy-kitten.jpg'));

        $imageSize = getimagesize(vfsStream::url('assets/w50/happy-kitten.jpg'));
        $this->assertEquals(50, $imageSize[0]);
    }

    /**
     * Test if an image can be processed
     *
     * @return void
     */
    public function testProcessHeight()
    {
        $this->object->process(vfsStream::url('assets/h50/happy-kitten.jpg'));
        $this->assertTrue($this->root->hasChild('h50'));
        $this->assertTrue($this->root->getChild('h50')->hasChild('happy-kitten.jpg'));

        $imageSize = getimagesize(vfsStream::url('assets/h50/happy-kitten.jpg'));
        $this->assertEquals(50, $imageSize[1]);
    }

    /**
     * Test if an image can be processed
     *
     * @return void
     */
    public function testProcessWithoutWidthAndHeight()
    {
        $this->setExpectedException('RuntimeException', 'Width and height or cropping needs to be defined');
        $this->object->process(vfsStream::url('assets/q100/happy-kitten.jpg'));
    }

    public function testProcessCrop()
    {
        $this->object->process(vfsStream::url('assets/c0,0,10,10/happy-kitten.jpg'));
        $this->assertTrue($this->root->hasChild('c0,0,10,10'));
        $this->assertTrue($this->root->getChild('c0,0,10,10')->hasChild('happy-kitten.jpg'));

        $imageSize = getimagesize(vfsStream::url('assets/c0,0,10,10/happy-kitten.jpg'));
        $this->assertEquals(10, $imageSize[1]);
    }

    /**
     * @runInSeparateProcess
     */
    public function testServe()
    {
        $dummy = new Image(null, 1, 1);
        $dummy->save(vfsStream::url('assets') . '/foo.jpg');

        // $this->expectOutputString('foo');
        $this->object->serve(vfsStream::url('assets/foo.jpg'));

        $headersList = xdebug_get_headers();
        $this->assertNotEmpty($headersList);
        $this->assertEquals(array(
              "Content-Type: image/jpeg",
              "Content-Length: 692",
              "Cache-Control: no-store, no-cache, must-revalidate, max-age=0",
              "Pragma: no-cache",
        ), $headersList);
    }
}