# Thumper - PHP Image Resizer
Thumper is an image handling class with support for [Laravel 4](http://four.laravel.com) integration. You give it some parameters and it creates a thumbnail image that you can display on your site. Thumper will also write a "cached" file to your asset directory so you can easily reuse the image.

Thumper's intention is to create a installable [composer](http://getcomposer.org/) package that behaves like [SLIR](https://github.com/lencioni/SLIR) or TimThumb.

**Build status**
[![Build Status](https://travis-ci.org/nckg/Thumper.png?branch=master)](https://travis-ci.org/nckg/Thumper)

## Install
### Composer side
Add `"nckg/thumper": "dev-master"` to the `require` section of your `composer.json`

    "require": {
        ...
        "nckg/thumper": "dev-master"
    },

### Laravel side

Add the following code to the `providers` section of the `app/config/app.php` file

```php
'Nckg\Thumper\ThumperServiceProvider',
```

so that it'll look something like the following

```php
'providers' => array(
    ...
    ...
    ...
    'Nckg\Thumper\ThumperServiceProvider',

),
```

and add the following code to the `aliases` section of the `app/config/app.php` file

```php
'Thumper' => 'Nckg\Thumper\Facades\Thumper',
```

so that it'll look something like the following

```php
'aliases' => array(

    ...
    ...
    ...
    'Thumper'   => 'Nckg\Thumper\Facades\Thumper',

),
```

## Usage
### Available parameters

- **w** = Maximum width
- **h** = Maximum height
- **f** = Fit
	- **0** - Resize to Fit specified dimensions (no cropping)
	- **1** - Crop and resize to best fit the dimensions (default behaviour)
	- **2** - Resize proportionally to fit entire image into specified dimensions, and add borders if required
	- **3** - Resize proportionally adjusting size of scaled image so there are no borders gaps
- **q** = Quality
- **c** = Crops the image to a specified rectangle. The input to this parameter should be 4 numbers for 'x,y,width,height' - for example, 'c10,20,200,250' would select the 200x250 pixel rectangle starting from 10 pixels from the left edge and  20 pixels from the top edge of the image.

####Examples

#####Default
######Code

    <img src="/assets/w100/foto.jpg" />

######Generates

![Image with a width of 100px](http://f.cl.ly/items/3N1g2c1E3l2C171P1k08/foto-1.jpg)

######Code

    <img src="/assets/w100-h150/foto.jpg" />

######Generates

![Image with a width of 100px and a height of 150px](http://f.cl.ly/items/3b010R1O3U1C3d2L0v2n/foto-2.jpg)

