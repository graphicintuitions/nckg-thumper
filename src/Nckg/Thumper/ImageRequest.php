<?php namespace Nckg\Thumper;

use Nckg\Thumper\Transformer;

class ImageRequest {
    /**#@+
     * Available options for fit
     */
    const REQUEST_PARAM_WIDTH = 'w';
    const REQUEST_PARAM_HEIGHT = 'h';
    const REQUEST_PARAM_FIT = 'f';
    const REQUEST_PARAM_QUALITY = 'q';
    const REQUEST_PARAM_CROP = 'c';
    /**#@-*/

    /**
     * Maximum width for resized image, in pixels
     *
     * @var integer
     */
    protected $width;

    /**
     * Maximum height for resized image, in pixels
     *
     * @var integer
     */
    protected $height;

    /**
     * Zoom crop option
     *
     * @var integer
     */
    protected $fit;

    /**
     * Quality of the image, from 0 to 100
     *
     * @var integer
     */
    protected $quality;

    /**
     * Crops the image to a specified rectangle. The input to this parameter should be
     * 4 numbers for 'x,y,width,height' - for example, 'c10,20,200,250' would
     * select the 200x250 pixel rectangle starting from 10 pixels from the left edge and
     * 20 pixels from the top edge of the image.
     *
     * @var array
     */
    protected $crop;

    /**
     * Path of the image
     *
     * @var integer
     */
    protected $path;

    /**
     *
     * @var Application|array
     */
    protected $app;

    /**
     * Active parameters
     *
     * @var array
     */
    protected $activeParams = array();

    /**
     * Constructor
     *
     * @param Application|array $app
     * @param Request $request
     */
    public function __construct($app)
    {
        $this->app = $app;

        // set default values
        $this->quality = $this->app['config']['nckg/thumper::quality'];
        $this->fit = $this->app['config']['nckg/thumper::fit'];
    }

    /**
     * Get the path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get the maximum width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Get the maximum height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get the quality
     *
     * @return integer
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     *
     *
     * @return integer
     */
    public function getFit()
    {
        return $this->fit;
    }

    /**
     *
     *
     * @return array
     */
    public function getCrop()
    {
        return $this->crop;
    }

    /**
     * Sets the path
     *
     * @param string $value
     */
    public function setPath($value)
    {
        $this->path = (string) $value;
    }

    /**
     * Set the width of the request
     *
     * @param integer $value
     */
    public function setWidth($value)
    {
        $this->width = (int) $value;

        if ($this->width < 1) {
            throw new \RuntimeException('Width must be greater than 0: ' . $this->width);
        }
    }

    /**
     * Set the height of the request
     *
     * @param integer $value
     */
    public function setHeight($value)
    {
        $this->height = (int) $value;

        if ($this->height < 1) {
            throw new \RuntimeException('Height must be greater than 0: ' . $this->height);
        }
    }

    /**
     * Set the fit option
     *
     * @param integer $value
     */
    public function setFit($value)
    {
        $this->fit = (int) $value;

        if ($this->fit < Transformer::RESIZE_TO_FIT) {
            throw new \RuntimeException('Zoom/crop can no be less than than 0: ' . $this->fit);
        }

        if ($this->fit > Transformer::RESIZE_AND_FIT_NO_BORDERS) {
            throw new \RuntimeException('Zoom/crop can not greater than 3: ' . $this->fit);
        }
    }

    /**
     * Set the crop values
     *
     * @param string $value
     */
    public function setCrop($value)
    {
        if (!strstr($value, ',')) {
            throw new \RuntimeException("The input to this parameter should be 4 numbers for 'x,y,width,height': " . $value);
        }

        $this->crop = explode(',', $value);
    }

    /**
     * Set the quality of the image
     *
     * @param integer $value
     */
    public function setQuality($value)
    {
        $this->quality = (int) $value;

        if ($this->quality < 1) {
            throw new \RuntimeException('Quality must be greater than 0: ' . $this->quality);
        }
    }

    /**
     * Init the request stuff
     *
     * @param  string $requestUri
     * @return void
     */
    public function init($requestUri)
    {
        // get parameters
        $params = $this->getParameters($requestUri);

        // Set image path first
        if (isset($params['i']) and $params['i'] != '' and $params['i'] != '/') {
            $this->set('i', $params['i']);
            unset($params['i']);
        } else {
            throw new \RuntimeException('Source image was not specified.');
        }

        // Set the rest of the parameters
        foreach ($params as $name => $value) {
            $this->set($name, $value);
        }

        $this->activeParams = $params;
    }

    /**
     * Get the parameters from the request
     *
     * @return array
     */
    private function getParameters($requestUri)
    {
        $params = array();

        // The parameters should be the first set of characters after the slir path
        $assetPath = preg_quote(basename($this->app['config']['nckg/thumper::asset_path']));
        $request = preg_replace('`.*?/' . $assetPath . '/`', '', (string) $requestUri, 1);
        $paramString = strtok($request, '/');

        if ($paramString === false or $paramString === $request) {
            throw new \RuntimeException('Not enough parameters were given.

                Available parameters:
                 w = Maximum width
                 h = Maximum height
                 f = Zoom/Crop
                 q = Quality

                Example usage:
                ' . $this->app['config']['nckg/thumper::asset_path'] . '/w300-h300/image.jpg'
            );
        }

        // The image path should start right after the parameters
        $params['i']  = substr($request, strlen($paramString) + 1); // +1 for the slash

        // The parameters are separated by hyphens
        $rawParam = strtok($paramString, '-');
        while ($rawParam !== false) {
            if (strlen($rawParam) > 1) {
                // The name of each parameter should be the first character of the parameter string
                // and the value of each parameter should be the remaining characters of the parameter string
                $params[$rawParam[0]] = substr($rawParam, 1);
            }

            $rawParam = strtok('-');
        }

        return $params;
    }

    /**
     * Generate a path
     *
     * @return string
     */
    public function toPath()
    {
        // value array
        $params = array(
            self::REQUEST_PARAM_WIDTH => 'width',
            self::REQUEST_PARAM_HEIGHT => 'height',
            self::REQUEST_PARAM_FIT => 'fit',
            self::REQUEST_PARAM_QUALITY => 'quality',
            self::REQUEST_PARAM_CROP => 'crop',
        );

        $keys = array_keys($this->activeParams);

        // loop params
        $result = array();
        foreach ($params as $key => $value) {
            if (!is_null($this->$value) and in_array($key, $keys)) {
                if (is_array($this->$value)) {
                    $result[] = $key . implode(',', $this->$value);
                } else {
                    $result[] = $key . $this->$value;
                }
            }
        }

        return implode('-', $result);
    }

    /**
     * Set values
     *
     * @param string $name
     * @param string $value
     */
    private function set($name, $value)
    {
        switch ($name) {
            case 'i':
                $this->setPath($value);
                break;

            case self::REQUEST_PARAM_WIDTH:
                $this->setWidth($value);
                break;

            case self::REQUEST_PARAM_HEIGHT:
                $this->setHeight($value);
                break;

            case self::REQUEST_PARAM_FIT:
                $this->setFit($value);
                break;

            case self::REQUEST_PARAM_CROP:
                $this->setCrop($value);
                break;

            case self::REQUEST_PARAM_QUALITY:
                $this->setQuality($value);
                break;

        }
    }
}