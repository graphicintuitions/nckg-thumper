<?php namespace Nckg\Thumper;

use Illuminate\Support\ServiceProvider;

class ThumperServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('nckg/thumper');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['config']->package('nckg/thumper', 'nckg/thumper', 'nckg/thumper');

        $this->app['thumper'] = $this->app->share(function($app)
        {
            return new Thumper(
                $app
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('thumper');
    }

}