<?php namespace Nckg\Thumper;

use Symfony\Component\HttpFoundation\Request;
use Intervention\Image\Image;

class Thumper {
    /**
     *
     * @var Application|array
     */
    protected $app;

    /**
     * Constructor
     *
     * @param Application|array $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Processes the image and writes it into the cache
     *
     * @param  string $filename
     * @return Image
     */
    public function process($filename)
    {
        $request = new ImageRequest($this->app);
        $request->init($filename);

        return $this->writeToCache($request);
    }

    /**
     * Writes an image into the cache with the correct sizes
     *
     * @param  ImageRequest $request [description]
     * @return Image
     */
    private function writeToCache(ImageRequest $request)
    {
        // If both the width and height haven't been passed along,
        // throw an exception.
        if ((is_null($request->getHeight()) and is_null($request->getWidth())) and is_null($request->getCrop())) {
            throw new \RuntimeException('Width and height or cropping needs to be defined');
        }

        if ((is_null($request->getHeight()) and is_null($request->getWidth())) and $request->getCrop()) {
            list($top, $left, $width, $height) = $request->getCrop();
            $request->setWidth($width);
            $request->setHeight($height);
        }

        // transform
        $transformer = new \Nckg\Thumper\Transformer(
            $request,
            new Image($this->app['config']['nckg/thumper::realpath'] . '/' . $request->getPath())
        );

        $img = $transformer->transform();

        // save
        $path = $this->app['config']['nckg/thumper::realpath'] . '/' . $request->toPath();

        // create the directory
        if (!is_dir($path)) {
            mkdir($path);
        }

        // set directory
        $img->dirname = $path;

        // save it
        $img->save(null, $request->getQuality());

        // return the image
        return $img;
    }

    /**
     * Serve an image
     *
     * @param  string $file
     * @return bool
     */
    public function serve($file)
    {
        $s = getimagesize($file);

        if (!($s and $s['mime'])) {
            return false;
        }

        // set headers
        header('Content-Type: ' . $s['mime']);
        header('Content-Length: ' . filesize($file));
        header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
        header('Pragma: no-cache');

        // echo image
        $bytes = @readfile($file);

        if ($bytes > 0) {
            return true;
        }

        $content = @file_get_contents($file);

        if ($content != false) {
            echo $content;
            return true;
        }

        return false;
    }
}