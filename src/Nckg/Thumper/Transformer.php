<?php namespace Nckg\Thumper;

use Nckg\Thumper\ImageRequest;
use Intervention\Image\Image;

/**
 * https://www.youtube.com/watch?v=nLS2N9mHWaw
 */
class Transformer {
    /**#@+
     * Available options for zoom/crop
     */
    const RESIZE_TO_FIT = 0;
    const CROP_AND_RESIZE = 1; // default
    const RESIZE_AND_FIT = 2;
    const RESIZE_AND_FIT_NO_BORDERS = 3;
    /**#@-*/

   /**
     * ImageRequest
     *
     * @var ImageRequest
     */
    protected $request;

    /**
     * Image
     *
     * @var Image
     */
    protected $image;

    /**
     * Constrcutor
     * @param ImageRequest $request
     * @param Image $image
     */
    final public function __construct(ImageRequest $request, Image $image)
    {
        $this->request = $request;
        $this->image = $image;
    }

    /**
     * Resizes and crop an image
     *
     * @return Intervention\Image\Image
     */
    final public function transform()
    {
        if ($this->request->getCrop()) {
            list($top, $left, $width, $height) = $this->request->getCrop();

            $this->image->crop($width, $height, $left, $top);
        }

        switch ($this->request->getFit()) {
            case self::RESIZE_TO_FIT:
                return $this->resizeToFit();
                break;
            case self::CROP_AND_RESIZE:
                return $this->cropAndResize();
                break;
            case self::RESIZE_AND_FIT:
                return $this->resizeAndFit();
                break;
            case self::RESIZE_AND_FIT_NO_BORDERS:
                return $this->resizeAndFitNoBorders();
                break;
        }
    }

    /**
     * Resize to Fit specified dimensions (no cropping)
     *
     * @return Intervention\Image\Image
     */
    private function resizeToFit()
    {
        return $this->image->resize($this->request->getWidth(), $this->request->getHeight(), false);
    }

    /**
     * Crop and resize to best fit the dimensions (default behaviour)
     *
     * @return Intervention\Image\Image
     */
    private function cropAndResize()
    {
        // set new width & height
        $newWidth = $this->request->getWidth() ? $this->request->getWidth() : $this->request->getHeight();
        $newHeight = $this->request->getHeight() ? $this->request->getHeight() : $this->request->getWidth();

        // only use height if the image is in landscape or square mode
        if ($newWidth < $newHeight or $newWidth == $newHeight) {
            $newWidth = null;
        }

        // only use height if the image is in portrait mode
        if ($newHeight < $newWidth) {
            $newHeight = null;
        }

        // return a resized image
        return $this->image->resize($newWidth, $newHeight, true)
            ->resizeCanvas($this->request->getWidth(), $this->request->getHeight(), 'center');
    }

    /**
     * Resize proportionally to fit entire image into specified dimensions,
     * and add borders if required
     *
     * @return Intervention\Image\Image
     */
    private function resizeAndFit()
    {
        // set new width & height
        $newWidth = $this->request->getWidth();
        $newHeight = $this->request->getHeight();

        // return a resized image
        return $this->image->resize($newWidth, $newHeight, true)
            ->resizeCanvas($newWidth, $newHeight, 'center', false, 'ffffff');
    }

    /**
     * Resize proportionally adjusting size of scaled image so there
     * are no borders gaps
     *
     * @return Intervention\Image\Image
     */
    private function resizeAndFitNoBorders()
    {
        return $this->image->resize($this->request->getWidth(), $this->request->getHeight(), true);
    }

}