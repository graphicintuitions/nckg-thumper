<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Debug mode or not
    |--------------------------------------------------------------------------
    |
    |
    */
    'debug' => true,

    /*
    |--------------------------------------------------------------------------
    | The path to your asset folder
    |--------------------------------------------------------------------------
    | This is the relative path to your asset folder.
    | If your assets folder is at "http://foo.bar/assets"
    | then you should put "/assets" here
    |
    */
    'asset_path' => '/assets',

    /*
    |--------------------------------------------------------------------------
    | The real path to your asset folder
    |--------------------------------------------------------------------------
    | So we know exactly where to store your precious files.
    |
     */
    'realpath' => realpath(__DIR__ . '../assets'),

    /*
    |--------------------------------------------------------------------------
    | Is a png image should be transparent
    |--------------------------------------------------------------------------
    | Define if a png image should have a transparent background color.
    | Use false value if you want to display a custom coloured canvas_colour
    |
    | NOTE: this doesn't work yet
     */
    'png_is_transparent' => false,

    /*
    |--------------------------------------------------------------------------
    | The default image quality
    |--------------------------------------------------------------------------
    | The default image quality as an integer. This can be
    | anything from 1 to 100
    |
     */
    'quality' => 90,

    /*
    |--------------------------------------------------------------------------
    | The default zoom/crop setting
    |--------------------------------------------------------------------------
    | Available options are:
    | RESIZE_TO_FIT
    | CROP_AND_RESIZE
    | RESIZE_AND_FIT
    | RESIZE_AND_FIT_NO_BORDERS
     */
    'fit' => Nckg\Thumper\Transformer::CROP_AND_RESIZE, // Default zoom/crop setting

    /*
    |--------------------------------------------------------------------------
    | Default canvas color
    |--------------------------------------------------------------------------
    | Defaults to white.
    |
     */
    'canvas_color' => 'ffffff', // Default canvas colour

);